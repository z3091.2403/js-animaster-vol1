(function main() {
  document.getElementById('fadeInPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeInBlock');
      animaster().fadeIn(block, 5000);
    });

  document.getElementById('fadeOutPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeOutBlock');
      animaster().fadeOut(block, 5000);
    });

  document.getElementById('moveAndHidePlay')
    .addEventListener('click', function () {
        const block = document.getElementById('moveAndHideBlock');
        animaster().moveAndHide(block, 1000, { x: 100, y: 20 });
    });

  document.getElementById('moveAndHideReset')
    .addEventListener('click', function () {
        const block = document.getElementById('moveAndHideBlock');
        animaster().resetMoveAndHide(block);
    });

  document.getElementById('movePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('moveBlock');
      animaster().move(block, 1000, { x: 100, y: 10 });
    });

  document.getElementById('scalePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('scaleBlock');
      animaster().scale(block, 1000, 1.25);
    });

  document.getElementById('showAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('showAndHideBlock');
      animaster().showAndHide(block, 5000);
    });

  let heartBeatingObj;

  document.getElementById('heartBeatingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('heartBeatingBlock');
      heartBeatingObj = animaster().heartBeating(block, 1000);
  });

  document.getElementById('heartBeatingStop')
    .addEventListener('click', function () {
      heartBeatingObj.stop();
  });

  let shakingObj;

  document.getElementById('shakingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('shakingBlock');
      shakingObj = animaster().shaking(block);
    });

  document.getElementById('shakingStop')
    .addEventListener('click', function () {
    shakingObj.stop();
    });
})();

function animaster() {
  function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
      result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
      result.push(`scale(${ratio})`);
    }
    return result.join(' ');
  }
  function resetFadeIn(element) {
    element.classList.remove('show');
    element.classList.add('hide');
    element.style.transitionDuration = null;
  }
  function resetFadeOut(element) {
    element.classList.remove('hide');
    element.classList.add('show');
    element.style.transitionDuration = null;
  }
  function resetMoveAndScale(element) {
    element.style.transitionDuration = null;
    element.style.transform = null;
  }

  return {
    fadeIn:
      /**
      * Блок плавно появляется из прозрачного.
      * @param element — HTMLElement, который надо анимировать
      * @param duration — Продолжительность анимации в миллисекундах
      */
      function fadeIn(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
      },
    fadeOut:
      /**
      * Блок плавно исчезает.
      * @param element — HTMLElement, который надо анимировать
      * @param duration — Продолжительность анимации в миллисекундах
      */
      function fadeOut(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('show');
        element.classList.add('hide');
      },
    move:
      /**
      * Функция, передвигающая элемент
      * @param element — HTMLElement, который надо анимировать
      * @param duration — Продолжительность анимации в миллисекундах
      * @param translation — объект с полями x и y, обозначающими смещение блока
      */
      function move(element, duration, translation) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(translation, null);
      },
    scale:
      /**
       * Функция, увеличивающая/уменьшающая элемент
       * @param element — HTMLElement, который надо анимировать
       * @param duration — Продолжительность анимации в миллисекундах
       * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
       */
      function scale(element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(null, ratio);
      },
    showAndHide:
      function showAndHide(element, duration) {
        this.fadeIn(element, duration / 3);
        setTimeout(() => { this.fadeOut(element, duration / 3) }, duration / 3);
      },
    moveAndHide: 
      function moveAndHide(element, duration, translation) {
        this.move(element, duration * 2/5, translation);
        setTimeout(() => {this.fadeOut(element, duration * 3/5)}, duration * 2/5)
      },
    heartBeating: 
      function heartBeating(element, duration) {
        let timer = setInterval(() => { scaleMove(element, duration)}, duration);
            
         function scaleMove (element, duration) {
            animaster().scale(element, duration/2, 1.4);
            setTimeout(() => {animaster().scale(element, duration/2, 1)}, duration/2);
        }   
        return {
          stop() {
            clearInterval(timer);
          }
        }
    },
    shaking:
      function shaking(element) {
        let timer = setInterval(() => { shakingMove(element) }, 500);
        function shakingMove(element) {
          animaster().move(element, 250, { x: 20, y: 0 });
          setTimeout(() => { animaster().move(element, 250, { x: 0, y: 0 }) }, 250);
        }
        return {
          stop() {
            clearInterval(timer);
          }
        }
    },
    resetMoveAndHide: 
      function resetMoveAndHide(element) {
        resetFadeOut(element);
        resetMoveAndScale(element);
      }
  };
}